package tp3;

public class VerserPleinDArgent implements Runnable{

	//Attribut
	private Compte compte;

	//Accesseur

	//Constructeur
	public VerserPleinDArgent(Compte c) {
		compte = c;
	}

	//M�thodes
	public void run() {
		for (int i = 0; i < 100; i++) {
			compte.verserArgent(2);	//Ajouter 2 euros dans le solde
			
			compte.afficherSolde();
			
			try {			//Pause de 50 ms entre chaque versement et retourner une erreur si il y a
				Thread.sleep(50);
			} catch (InterruptedException e) {
				System.out.print("Erreur");
				e.printStackTrace();
			}
		}

	}

}
