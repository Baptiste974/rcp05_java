package tp3;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConvertisseurEuroDollar extends JFrame implements ActionListener{
	private Container panneau;
	private JButton calculer;
	private JButton quitter;
	private JTextField textEuro;
	private JTextField textDollar;
	private JTextField textTaux;
	private JLabel euro;
	private JLabel dollar1;
	private JLabel dollar2;
	private JLabel taux;
	private int f =0;	//Sens de convertion


	public ConvertisseurEuroDollar(){
		//Cr�ation de la fen�tre
		super("Mon Application Graphique");
		setSize(300, 150);
		setLocation(20,20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new GridLayout(3,6));

		//Cr�ation des panels
		JPanel panelHaut = new JPanel();
		JPanel panelMilieu = new JPanel();
		JPanel panelBas = new JPanel();	

		//Cr�ation des objets � utiliser
		calculer = new JButton("Calculer");
		quitter = new JButton("Quitter !");
		textEuro = new JTextField("");
		textDollar = new JTextField("");
		textTaux  = new JTextField("");
		euro = new JLabel("�      =>   ");
		dollar1 = new JLabel("$");
		dollar2 = new JLabel("$");
		taux = new JLabel("Taux 1� = ");
		
		//R�partition des objets par panel
		panelHaut.setLayout(new GridLayout(0, 4));
		panelHaut.add(textEuro);
		panelHaut.add(euro);
		panelHaut.add(textDollar);
		panelHaut.add(dollar1);


		panelMilieu.setLayout(new GridLayout(0, 3));
		panelMilieu.add(taux);
		panelMilieu.add(textTaux);
		panelMilieu.add(dollar2);

		panelBas.setLayout(new GridLayout(0, 2));
		panelBas.add(calculer);
		panelBas.add(quitter);

		//Ajout des panel au tableau
		panneau.add(panelHaut);
		panneau.add(panelMilieu);
		panneau.add(panelBas);


		calculer.addActionListener(this);
		quitter.addActionListener(this);

		euro.addMouseListener(new MouseListener() {


			public void mouseReleased(MouseEvent e) {	//Quand on laisse appui� le clic souris on d�place et relache
				euro.setText("�      =>   ");
				f = 0;

			}

			public void mousePressed(MouseEvent e) {	//Quand le clic souris reste appui�


			}

			public void mouseExited(MouseEvent e) {		//Quand la souris sort de la zone de mon label


			}

			public void mouseEntered(MouseEvent e) {	//Quand la souris passe dans la zone de mon label


			}

			public void mouseClicked(MouseEvent e) {	//Quand la souris clic dans la zone de mon label
				euro.setText("�      <=   ");
				f = 1;
			}
		});

		setVisible(true);
	}


	public static void main(String[] args) {
		ConvertisseurEuroDollar n = new ConvertisseurEuroDollar();
	}

	public void calculerEuroDollar(){
		double e = Double.parseDouble(textEuro.getText());
		double t = Double.parseDouble(textTaux.getText());
		double d;

		d = e*t;

		textDollar.setText(String.valueOf(d));	//Place la valeur de d dans le champ de texte textDollar
	}

	public void calculerDollarEuro(){
		double d = Double.parseDouble(textDollar.getText());
		double t = Double.parseDouble(textTaux.getText());
		double e;

		e = d/t;

		textEuro.setText(String.valueOf(e));	//Place la valeur de e dans le champ de texte textEuro
	}

	public void actionPerformed(ActionEvent c) {
		if (c.getSource() == calculer) {	//Compare l� ou l'�v�nement se fait pour v�rifier sur quel bouton on a click�
			if (f == 0) {
				calculerEuroDollar();
			}
			if (f == 1) {
				calculerDollarEuro();
			}
		}
		//Quitte quand on appui sur quiter
		if (c.getSource() == quitter) {
			System.exit(0);
		}
	}
}
