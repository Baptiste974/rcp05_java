package tp3;

public class Compteur1 extends Thread {

	private String nom;

	public Compteur1(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]){
		Compteur1 t1, t2, t3;
		t1=new Compteur1("Hello ");
		t2=new Compteur1("World ");
		t3=new Compteur1("and Everybody ");

		t1.start();
		t2.start();
		t3.start();

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			System.out.print("Erreur");
			e.printStackTrace();
		}

		System.out.println("Fin du programme");

		System.exit(0);

	}

}
//Exercice 1.1:
//Le nombre d'affichage ne correspond pas au nombre voulu.
//Le System.exit(0) va arr�ter le progamme principal et arr�ter les autres threads

//Exercices 1.2
//Les 3 threads ont plus de temps pour faire les affichages des noms

//Exercice 1.3
//L'affichage de fin programme s'affiche � la fin des threads car les threads ont eu le temps d'afficher les noms
