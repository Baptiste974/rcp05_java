//Exercice 1.4

package tp3;

public class Compteur1bis implements Runnable {

	private String nom;

	public Compteur1bis(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]){
		Compteur1bis t1, t2, t3;
		t1=new Compteur1bis("Hello ");
		t2=new Compteur1bis("World ");
		t3=new Compteur1bis("and Everybody ");
		Thread t1bis = new Thread(t1);
		Thread t2bis = new Thread(t2);
		Thread t3bis = new Thread(t3);

		t1bis.start();
		t2bis.start();
		t3bis.start();
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			System.out.print("Erreur");
			e.printStackTrace();
		}

		System.out.println("Fin du programme");
		
		System.exit(0);

	}
}
