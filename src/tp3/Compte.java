package tp3;

public class Compte {
	//Attributs
	private double solde = 0;
	private String nom;

	//Accesseur
	
	//Constructeur
	public Compte(String n) {
		this.nom = n;
	}
	
	//M�thodes
	public synchronized void verserArgent(double s) {	//synchronized va permettre au premier thread de finir sa t�che et ensuite permettre au deuxi�me thread
		this.solde = this.solde + s;					//qui utilise la m�me m�thode de commencer sa t�che
	}
	
	public synchronized void afficherSolde() {
		System.out.println("Voici le solde apr�s versement sur le compte " + this.nom + " : " + this.solde);
	}
}
