package tp3;

public class Compteur2 extends Thread{
	
	private String nom;

	public Compteur2(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]) {
		Compteur2 t1, t2, t3;
		t1=new Compteur2("Hello ");
		t2=new Compteur2("World ");
		t3=new Compteur2("and Everybody ");

		t1.start();
		t2.start();
		t3.start();

		try {
			t1.join();
			t2.join();	//Exercice 2.2	Ajout d'un join() pour t2 et t3 pour que le thread principal se termine apr�s les threads t1, t2 et t3
			t3.join();	//Exercice 2.2	Ajout d'un join() pour t2 et t3 pour que le thread principal se termine apr�s les threads t1, t2 et t3
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Fin du programme");
		
		System.exit(0);

	}
}

//Exercice 2.1 :
//Le Programme se termine bien apr�s la fin du thread t1 mais on attend pas la fin du t2 et t3 qui se termine au moment o� le thread principal se termine
