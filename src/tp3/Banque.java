package tp3;

public class Banque {

	public static void main(String[] args) {
		//Cr�ation des comptes
		Compte c1 = new Compte("Compte 1");
		Compte c2 = new Compte("Compte 2");
		VerserPleinDArgent v1c2 = new VerserPleinDArgent(c2);
		VerserPleinDArgent v2c2 = new VerserPleinDArgent(c2);
		
		//Cr�ation des threads
		Thread tc2n1 = new Thread(v1c2);
		Thread tc2n2 = new Thread(v2c2);
		
		//Versement d'argent dans les comptes
		c1.verserArgent(2);
		
		//Affichage des soldes des comptes
		c1.afficherSolde();
		
		//Start des threads
		tc2n1.start();		//Si les 2 threads ex�cutent une action sur le m�me compte les deux threads vont aller jusqu'� faire les 100 fois le versement.
		tc2n2.start();
	}

}
