package tp2;

public class FruitPiece extends Fruit implements VendreALaPiece{


	//attribut

	//accesseur

	//constructeur
	public FruitPiece(String n, double a, double v, double q, String p) {
		super(n, a, v, q, p);
	}

	public void vendrePiece(int q, Magasin m) {
		if (this.quantiteProduit<q) {
			System.out.println("Impossible de retirer la quantit� demand�e car la quantit� demand�e est sup�rieur au stock de " + getNom());
		} else {
			System.out.println("Quantit� du produit " + getNom() + " avant vente : " + getQuantiteProduit());
			this.quantiteProduit = this.quantiteProduit - q;
			System.out.println("Quantit� du produit " + getNom() + " apr�s vente : " + getQuantiteProduit());

			//Ajout du prix de vente de l'objet dans le capital
			System.out.println("Total du capital du magasin " + m.getNom() + " avant vente : " + m.getCapital());
			m.capital = m.capital + (this.prixVente * q);
			System.out.println("Total du capital du magasin " + m.getNom() + " apr�s vente : " + m.getCapital());
		}

	}

	//m�thode

}
