package tp2;

public class TestMagasin {

	public static void main(String[] args) {

		//Cr�ation des produits
		FruitVrac f1 = new FruitVrac("Banane", 1.5, 2.5, 3.6, "France");
		FruitPiece f2 = new FruitPiece("Ananas", 1, 2.5, 50, "Espagne");
		Electromenager e1 = new Electromenager("T�l�vision", 55.99, 70.99, 1, 135);

		//Cr�ation du magasin
		Magasin m1 = new Magasin("Turkey Ent");

		//Description des produits
		f1.seDecrire();
		f2.seDecrire();
		e1.seDecrire();


		//Ajout des produit en stock
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nAjout d'une quantit� du produit en plus\n");
		e1.remplirStock(5,m1);
		e1.seDecrire();

		//Retrait des produits en stock
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nRetrait d'une quantit� du produit\n");
		e1.retirerStock(2);
		e1.seDecrire();

		//Ajout des produit dans le magasin
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nAjout du produit dans le magasin\n");
		m1.ajoutProduit(f1);
		m1.ajoutProduit(f2);
		m1.ajoutProduit(e1);


		//Vendre les produits par le poids avec une inf�rieur sup�rieur au stock
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nVente de produit avec ajout dans le capital du magasin\n");
		f1.vendrePoids(2.2,m1);

		//Vendre les produits par unit� avec une quantit� inf�rieur au stock
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nVente de produit avec ajout dans le capital du magasin\n");
		f2.vendrePiece(5,m1);
		e1.vendrePiece(2,m1);

		//Vendre les produits par le poids ou � l'unit� avec une quantit� sup�rieur au stock
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nVente de produit avec ajout dans le capital du magasin\n");
		f1.vendrePoids(10.2,m1);

		//V�rification de l'attribution des soldes sur des produits
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nAttribution du solde\n");
		e1.solderProduit(50);
		f2.solderProduit(10);


		//V�rification de l'arr�t des soldes sur des produits
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nArr�t du solde\n");
		e1.arretSoldeProduit();
		f2.arretSoldeProduit();
		
		//Affichage des produits dans la liste du magasin
		System.out.println("\n////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("\nListe des produits\n");
		m1.listerProduit();
		
	}


}
