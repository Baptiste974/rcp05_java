package tp2;

public abstract class Produit {
	//attribut
	protected String nom;
	protected double prixVente;
	protected double prixAchat;
	protected double quantiteProduit;
	protected double solde;
	protected double prixVenteHorsSolde;	//Garder la valeur du prix de vente initial

	//accesseur
	public String getNom() {
		return this.nom;
	}

	public double getPrixVente() {
		return this.prixVente;
	}

	public double getAchatPrix() {
		return this.prixAchat;
	}

	public double getQuantiteProduit() {
		return this.quantiteProduit;
	}

	//constructeur
	public Produit(String n, double a, double v, double q) {
		this.nom = n;
		this.prixAchat = a;
		this.prixVente = v;
		this.quantiteProduit = q;
	}

	//m�thode
	public void seDecrire() {

	}

	public void remplirStock(double remplir, Magasin m) {
		this.quantiteProduit = this.quantiteProduit + remplir;
		//Ajout du prix de vente de l'objet dans le capital
		System.out.println("Total du capital du magasin " + m.getNom() + " avant remplissage de " + remplir + " produit(s) :"  + m.getCapital());
		m.capital = m.capital - (this.prixAchat * remplir);
		System.out.println("Total du capital du magasin " + m.getNom() + " apr�s remplissage de " + remplir + " produit(s) :" + m.getCapital());

	}

	public void retirerStock(double retirer) {
		this.quantiteProduit = this.quantiteProduit - retirer;
	}
	
	//Cr�ation d'une m�thode pour solder un produit
	public void solderProduit(double s) {	//Entrer en param�tre le % du solde souhait�
		this.solde = s;
		this.prixVenteHorsSolde = this.prixVente;
		System.out.println("Prix de vente du produit " + this.getNom() + " avant solde de " + solde + "% : " + this.getPrixVente());
		this.prixVente = this.prixVente - (this.prixVente * (s/100));
		System.out.println("Prix de vente du produit " + this.getNom() + " apr�s solde de " + solde + "% : " + this.getPrixVente());
	}
	
	//Cr�ation d'une m�thode pour arr�ter les solde sur un produit
	public void arretSoldeProduit() {
		System.out.println("Prix de vente du produit " + this.getNom() + " avant arret des soldes de " + solde + "% : " + this.getPrixVente());
		this.prixVente = this.prixVenteHorsSolde;
		System.out.println("Prix de vente du produit " + this.getNom() + " apr�s arr�t des soldes de " + solde + "% : " + this.getPrixVente());
	}
}

