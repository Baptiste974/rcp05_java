package tp2;

import java.util.Vector;

public class Magasin {
	//attribut
	private String nom;
	protected double capital = 1000;
	private Vector<Produit> listeProduit;	//Le vecteur contiendra que des produits
	
	//accesseur
	public double getCapital() {
		return this.capital;
	}
	
	public String getNom() {
		return this.nom;
	}

	//constructeur
	public Magasin(String n) {
		this.nom = n;
		listeProduit = new Vector<>();
	}
	
	//m�thode
	public void ajoutProduit(Produit p) {
		listeProduit.addElement(p);		//Ajout du prioduit dans la liste
		System.out.println("Voici la liste des produits du magasin " + this.nom);
		
		for (int i=0; i<listeProduit.size(); i++) {		//Affiche la liste des produits
			System.out.println(listeProduit.elementAt(i).getNom());
			System.out.println("Quantit� du produit " + listeProduit.elementAt(i).getNom() + " : " + listeProduit.elementAt(i).getQuantiteProduit());	//Affiche la quantit� li� au produit choisi
		}
	}
	
	public void listerProduit() {
		for (int i=0; i<listeProduit.size(); i++) {		//Affiche la liste des produits
			System.out.println(listeProduit.elementAt(i).getNom());
			System.out.println("Quantit� du produit " + listeProduit.elementAt(i).getNom() + " : " + listeProduit.elementAt(i).getQuantiteProduit());	//Affiche la quantit� li� au produit choisi
		}
	}
	
	
}
