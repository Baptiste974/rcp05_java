package tp2;

public class Electromenager extends Produit implements VendreALaPiece{

	//attribut
	private int codeBarre;

	//accesseur
	public int getCodeBarre() {
		return this.codeBarre;
	}

	//constructeur
	public Electromenager(String n, double a, double v, double q, int c) {
		super(n, a, v, q);
		this.codeBarre = c;
	}
	//m�thode

	public void seDecrire() {
		System.out.println("Nom du produit : " + this.nom);
		System.out.println("Prix d'achat (euros): " + this.prixAchat);
		System.out.println("Prix de vente (euros): " + this.prixVente);
		System.out.println("Qauntit� du produit : " + this.quantiteProduit);
		System.out.println("Ce produit � comme code barre : " + this.codeBarre);
	}

	public void vendrePiece(int q, Magasin m) {
		if (this.quantiteProduit<q) {
			System.out.println("Impossible de retirer la quantit� demand�e car la quantit� demand�e est sup�rieur au stock de " + getNom());
		} else {
			System.out.println("Quantit� du produit " + getNom() + " avant vente : " + getQuantiteProduit());
			this.quantiteProduit = this.quantiteProduit - q;
			System.out.println("Quantit� du produit " + getNom() + " apr�s vente : " + getQuantiteProduit());
			
			//Ajout du prix de vente de l'objet dans le capital
			System.out.println("Total du capital du magasin " + m.getNom() + " avant vente : " + m.getCapital());
			m.capital = m.capital + (this.prixVente * q);
			System.out.println("Total du capital du magasin " + m.getNom() + " apr�s vente : " + m.getCapital());
		}

	}
}
