package tp2;

public abstract class Fruit extends Produit{

	//attribut
	protected String pays;

	//accesseur
	public String getPays() {
		return this.pays;
	}

	//constructeur
	public Fruit(String n, double a, double v, double q,String p) {
		super(n, a, v, q);
		this.pays = p;
	}

	//m�thode
	public void seDecrire() {
		System.out.println("Nom du produit : " + this.nom);
		System.out.println("Prix d'achat (euros): " + this.prixAchat);
		System.out.println("Prix de vente (euros): " + this.prixVente);
		System.out.println("Qauntit� du produit : " + this.quantiteProduit);
		System.out.println("Ce fruit vient de : " + this.pays);
	}
}
