package TD1et2.Exercice2;

import java.awt.Point;

public abstract class FormeGeometrique {
	//attributs
	Point p;
	
	//constructeurs
	
	//m�thodes
	public void deplacer() {
		System.out.println("Je me d�place");
	}
	
	public abstract float CalculerPerimetre();
	public abstract float CalculerSurface();
	public abstract void seDecrire();

}
