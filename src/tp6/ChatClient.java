package tp6;

import javax.swing.*;

import javafx.scene.transform.Affine;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatClient extends JFrame implements ActionListener, Runnable{

	/*___________ Attributs ______________*/
	private JTextField messageAEnvoyer;		//Zone de saisie du message
	private JButton b_Envoyer;				//Le bouton d'envoi du message
	private Socket maSocket;				//Socket du programme client
	private int numeroPort = 8888;			//Port 
	private String adresseServeur = "localhost";//Adresse du serveur
	private PrintWriter writerClient;		//Objet permettant l'�criture de message sur le socket
	private BufferedReader readerClient;
	private JTextArea affichageClient;

	/*___________ Constructeur ______________*/
	public ChatClient(){
		//D�finition de la fen�tre
		super("Client - Panneau d'affichage");
		setSize(400, 300);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		messageAEnvoyer = new JTextField(20);
		b_Envoyer = new JButton("Envoyer");
		b_Envoyer.addActionListener(this);
		affichageClient = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(affichageClient, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		affichageClient.setEditable(false);
		
		//Cr�ation JPanel
		JPanel zoneDeSaisie = new JPanel();
		zoneDeSaisie.setLayout(new GridLayout(1, 1));
		zoneDeSaisie.add(messageAEnvoyer);

		//Disposition des composants graphiques
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(scrollPane,BorderLayout.CENTER);
		pane.add(zoneDeSaisie,BorderLayout.NORTH);
		pane.add(b_Envoyer, BorderLayout.SOUTH);

		//Affichage d'un message de bienvenue
		afficherClient("Le panneau est actif\n");

		//Cr�ation du Socket client
		try {
			maSocket = new Socket(adresseServeur, numeroPort);
			writerClient = new PrintWriter(maSocket.getOutputStream()); 
		} catch (Exception e) {
			System.out.println("Erreur Cr�ation client");
		}
		
		
		//D�marrage de l'�coute du retour serveur
		Thread t = new Thread(this);
		t.start();
		
		//Affichage de la fen�tre
		setVisible(true);

		
		
	}

	/*___________ M�thodes ______________*/

	public void actionPerformed(ActionEvent e) {
		//Clic du bouton Envoyer d�clanche la m�thode emettre()
		if (e.getSource() == b_Envoyer) {
			emettre();

		}

	}

	public void emettre() {
		//Envoi du message
		try {
			String message = messageAEnvoyer.getText(); 
			writerClient.println(message); //Envoi du texte par l'objet writer
			writerClient.flush();
			messageAEnvoyer.setText("");
			messageAEnvoyer.requestFocus();

		} catch (Exception e) {
			afficherClient("Erreur Envoi du Message");
		}
	}

	private void ecouterServeur() {	//R�cup�ration des informations renvoy� par le serveur
		try {
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));

			String retour; 

			while ((retour = readerClient.readLine()) != null){ 
				afficherClient("Message recu du Serveur: " + retour + "\n");
			}
		} catch (Exception e) {
			afficherClient("Probl�me de r�ception");
		}
	}
	
	public void afficherClient(String str){
		affichageClient.append(str);
	}
	
	public void run() {
		try {
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));

			String retour; 

			while ((retour = readerClient.readLine()) != null){ 
				afficherClient("Message recu du Serveur: " + retour + "\n");
			}
		} catch (Exception e) {
			afficherClient("Probl�me de r�ception");
		}
	}

	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		new ChatClient();
	}

	

}
