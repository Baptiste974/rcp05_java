package tp6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class EcouteClient extends Thread{
	//attribut
	private ChatServeur serveur;
	private BufferedReader readerSVR;
	private PrintWriter writerSVR;

	//Constructeur
	public EcouteClient (ChatServeur svr, BufferedReader r, PrintWriter w) {
		this.serveur = svr;
		this.readerSVR = r;
		this.writerSVR = w;
		
		String ligne; 
		try {
			while ((ligne = readerSVR.readLine()) != null){ 
				serveur.afficherPanneau("Message recu du Client: " + ligne + "\n");

				//Renvoi du message re�u au client

				writerSVR.println(ligne);
				writerSVR.flush();
			}
		} catch (Exception e) {
			serveur.afficherPanneau("Connexion Termin�e");
		}
	}

	//accesseur

	//m�thode
	public void run() {

	}
}
