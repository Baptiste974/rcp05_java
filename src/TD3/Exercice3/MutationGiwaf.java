package TD3.Exercice3;

public interface MutationGiwaf {
	public int monNbYeux();
	public String maForme();
	public String maCoulleur();

	public abstract int vitesseDeplacement() throws NeSeDeplacePasExeption;
}
