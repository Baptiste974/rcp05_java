package TD3;

public class HelloworldImplementRunnable implements Runnable{
	protected String nom;
	
	public HelloworldImplementRunnable(String nom) {
		this.nom = nom;
	}
	
	public void run() {
		for (int i = 1; i <= 10; i++) {
			try {
				Thread.sleep(10);
			}
			catch(InterruptedException e) {
				System.err.println(nom + " a �t� interrompu.");
			}
			System.out.println(nom);
		}
	}
	
	public static void main (String[] args){
		HelloworldImplementRunnable hello = new HelloworldImplementRunnable("Hello");
		HelloworldImplementRunnable world = new HelloworldImplementRunnable("world");
		Thread my_thread1 = new Thread(hello);
		Thread my_thread2 = new Thread(world);
		my_thread1.start();
		my_thread2.start();
	}
}
