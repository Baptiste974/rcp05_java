package tp4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.DirectoryIteratorException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javafx.geometry.Side;


public class LaFourmiDeLangton extends JFrame implements ActionListener {
	//Attribut
	private Container panneau;
	private JButton next;
	private JLabel nbTour;
	private int n = 0;
	private int dimension = 100;
	private JTextField plateau[][];

	//Coordonn�es initiale pour la fourmi
	int x = dimension/2;
	int y = dimension/2;

	//Direction pour la fourmi
	int direction = 0;

	//Constructeur
	public LaFourmiDeLangton() {
		//Cr�ation de la fen�tre
		super("La Fourmi de Langton");
		setSize(1000, 1000);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new BorderLayout());


		//Cr�ation des objets � utiliser
		next = new JButton("Next");
		nbTour = new JLabel("Nombre de tour = " + n);



		//Ajout des �l�ments au tableau
		panneau.add(nbTour, BorderLayout.NORTH);
		panneau.add(this.creerPlateau(),BorderLayout.CENTER);
		panneau.add(next,BorderLayout.SOUTH);	

		//Placement de la fourmi sur le damier
		this.initFourmi();

		next.addActionListener(this);


		setVisible(true);
		this.automatique();	//Faire se d�placer la fourmi automatiquement
	}

	//M�thodes
	public JPanel creerPlateau() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(dimension,dimension)); 	//D�coupage de p
		plateau = new JTextField[dimension][dimension];

		for (int i = 0; i < dimension; i++) {	//D�coupage pour faire notre damier
			for (int j = 0; j < dimension; j++) {
				JTextField f = new JTextField();
				plateau[i][j] = f;
				plateau[i][j].setBackground(Color.WHITE);
				p.add(f);
			}
		}
		return p;
	}

	public void initFourmi() {
		plateau[x][y].setText("" + direction);

	}

	public void next() {
		if (plateau[x][y].getBackground() == Color.WHITE) {
			plateau[x][y].setBackground(Color.BLACK);	//Change la couleur de la case en noir

			if (direction == 0) {	//Si regarde en haut
				direction = 270;	//Regarde � gauche
				y = y - 1;
				n ++;				//Augmente le nombre de tour de 1
			}else if (direction == 90) {//Si regarde � droite
				direction = 0;		//Regarde en haut
				x = x-1;
				n ++;

			}else if (direction == 180) {	//Si regarde en bas
				direction = 90;		//Regarde � droite
				y = y+1;
				n ++;
			}else if (direction == 270) {	//Si regarde � gauche
				direction = 180;	//Regarde en bas
				x = x+1;
				n ++;
			}


		}

		if (plateau[x][y].getBackground() == Color.BLACK) {
			plateau[x][y].setBackground(Color.WHITE);
			if (direction == 0) {	//Si regarde en haut
				direction = 90;	//Regarde � droite
				y = y + 1;
				n ++;
			}else if (direction == 90) {	//Si regarde � droite
				direction = 180;		//Regarde en bas
				x = x+1;
				n ++;
			} else if (direction == 180) {	//Si regarde en bas
				direction = 270;		//Regarde � gauche
				y = y-1;
				n ++;
			} else if (direction == 270) {	//Si regarde � gauche
				direction = 0;	//Regarde en haut
				x = x-1;
				n ++;
			}

		}
		nbTour.setText("Nombre de tours : " + n);
		plateau[x][y].setText("" + direction);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == next) {
			System.out.println("clic");
			for (int i = 1; i < 1; i++) {		//Faire un certain nombre de fois la m�thode next()
				next();
			}

		}

	}

	public void automatique() {		//D�placement automatique de la fourmi sur le damier


		try {
			while (true) {
				next();
				Thread.sleep(2);
			}
		} catch (InterruptedException e) {
			System.out.print("Erreur");
		}


	}

	public static void main(String[] args) {
		LaFourmiDeLangton l = new LaFourmiDeLangton();
	}



}
