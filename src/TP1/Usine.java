package TP1;

import java.util.Vector;

public class Usine {
	//attributs
	private String nom;
	private int nombreStuart;
	private Vector<Stuart> liste;
	private double coffre = 0;
	
	
	//accesseurs
	public int getNombreStuart(){
		this.nombreStuart = liste.size();
		return this.nombreStuart;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public double getCoffre() {
		return this.coffre;
	}
	
	public int getListe() {
		int Liste;
		Liste = liste.size();
		return Liste;
	}
	
	//constructeurs
	public Usine(String n) {
		this.nom=n;
		liste = new Vector<Stuart>();
	}
	
	
	//m�thodes
	public void engagerStuart(Stuart s) {
		liste.addElement(s); //Ajout du stuart � la liste
		System.out.println("Voil� la liste des stuart engager par l'usine " + this.nom);
		
		for (int i = 0; i < liste.size(); i++) { 
			System.out.println(liste.elementAt(i).getNom()); //Affiche le contenu du vecteur
		} 
	}
	
	public void collecterImpot() {	//Calcul des imp�ts � ajouter � l'usine
		for (int i = 0; i < liste.size(); i++) {
			double bourseStuart = liste.elementAt(i).getBourse();
			
			double collect = bourseStuart*0.5;
			this.coffre = this.coffre+collect;
			
			liste.elementAt(i).bourseGrossepiece = liste.elementAt(i).bourseGrossepiece - collect; //retrait de la somme d'imp�t de la bourse du stuart
		}
	}
	
	public void tavaillerStuart() {	//Faire travailler le stuart pour tant d'ann�e
	
		for (int i = 0; i < liste.size(); i++) {
			System.out.println("Etat du coffre de l'usine " + this.nom + " avant la collect du stuart " + liste.elementAt(i).getNom() + " : "+ this.coffre + "pi�ce(s) dans le coffre");
			liste.elementAt(i).travailler();
			if (liste.elementAt(i).getCarriere()==0) {
				this.coffre = coffre + (liste.elementAt(i).getBourse()*0.2);	//Ajout de 20% de la bourse d'un stuart au coffre quand le stuart quitte la liste
				liste.removeElementAt(i);
			}
			collecterImpot();	//Ajout de 50% de la bourse d'un stuart au coffre et collect de l'imp�t de la bourse du stuart
			System.out.println("Etat du coffre de l'usine " + this.nom + " apr�s la collect " + liste.elementAt(i).getNom() + " : " + this.coffre + "pi�ce(s) dans le coffre");
		}
			
	}
	
	public void travaillerEmpire() {	//Faire travailler les stuarts pour le programme MonBelEmpire
		for (int i = 0; i < liste.size(); i++) {
			System.out.println("Etat du coffre de l'usine " + this.nom + " avant la collect du stuart " + liste.elementAt(i).getNom() + " : "+ this.coffre + "pi�ce(s) dans le coffre");
			liste.elementAt(i).travailler();
			if (liste.elementAt(i).getCarriere()==0) {
				this.coffre = coffre + (liste.elementAt(i).getBourse()*0.2);	//Ajout de 20% de la bourse d'un stuart au coffre quand le stuart quitte la liste
				liste.removeElementAt(i);
			}
			System.out.println("Etat du coffre de l'usine " + this.nom + " apr�s la collect " + liste.elementAt(i).getNom() + " : " + this.coffre + "pi�ce(s) dans le coffre");
		}
	}
	
	public void recruterStuartEmpire(Stuart s) {
		liste.addElement(s); //Ajout du stuart � la liste
		System.out.println("Voil� la liste des stuart engager par l'usine " + this.nom);
		
		for (int i = 0; i < liste.size(); i++) { 
			System.out.println(liste.elementAt(i).getNom()); //Affiche le contenu du vecteur
		}
		this.coffre = this.coffre - 50;
	}
}
