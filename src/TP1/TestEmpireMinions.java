package TP1;

public class TestEmpireMinions {

	public static void main(String[] args) {
	
		
	//Cr�ation de stuart
	Stuart s1 = new Stuart();
	Stuart s2 = new Stuart();
	Stuart s3 = new Stuart();
	s1.sePresenter();
	s2.sePresenter();
	s3.sePresenter();
	
	//Cr�ation d'usine
	Usine u1 = new Usine("Stark");
	
	//Engagement de stuart
	u1.engagerStuart(s1);
	u1.engagerStuart(s2);
	u1.engagerStuart(s3);
	
	
	//Faire travailler les stuarts
	u1.tavaillerStuart();
	}

}
