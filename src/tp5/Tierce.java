package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import TP1.Stuart;
import javafx.scene.layout.Border;

public class Tierce extends JFrame implements ActionListener, JugeDeCourse{

	/////////////////////////////////////Application graphique//////////////////////////////////////////////////
	//Attribut
	private Container panneau;
	private JButton go;
	private JLabel resultat;
	private Vector<Cheval> listeCheval;
	private int nbcheval = 6;
	private int nbBar = nbcheval;
	private Vector<JProgressBar> listeProgressBar;

	//Constructeur
	public Tierce() {

		//Cr�ation de la fen�tre
		super("Le Tierc�");
		setSize(300, 250);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new BorderLayout());

		//Cr�ation des objets � utiliser
		this.go = new JButton("Go!");
		this.resultat = new JLabel("R�sultat : ");


		//Ajout des �l�ments au tableau
		JPanel progressBar = new JPanel();
		progressBar.setLayout(new GridLayout(nbcheval, 1));
		
		panneau.add(this.resultat, BorderLayout.NORTH);
		panneau.add(progressBar,BorderLayout.CENTER);
		panneau.add(this.go,BorderLayout.SOUTH);

		go.addActionListener(this);

		//Vecteur pour stocker les chevaux
		listeCheval = new Vector<Cheval>();
		listeProgressBar = new Vector<JProgressBar>();
		
		//Cr�ation des bars de progression pour la course
		for (int i = 0; i < nbBar; i++) { //Jusqu'au nombre de bar voulu
			listeProgressBar.addElement(new JProgressBar());
			listeProgressBar.elementAt(i).setMaximum(50);
			listeProgressBar.elementAt(i).setMinimum(0);
			listeProgressBar.elementAt(i).setValue(0);
			listeProgressBar.elementAt(i).setStringPainted(true);
			progressBar.add(listeProgressBar.elementAt(i));
		}
		
		//Cr�ation des chevaux
		for (int i = 0; i < nbcheval; i++) {//Jusqu'au nombre de cheval voulu
			Cheval cheval;
			cheval = new Cheval(i, 50, this,listeProgressBar.elementAt(i));
			listeCheval.addElement(cheval);

		}
		
		
		

		setVisible(true);
	}

	public static void main(String[] args) {
		Tierce t = new Tierce();
	}


	//action du bouton
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == go) {
			for (int i = 0; i < listeCheval.size(); i++) {
				listeCheval.elementAt(i).start();
			}
		}

	}


	public synchronized void passeLaLigneDArrivee(int id) {
		String texte = resultat.getText();
		resultat.setText(texte + "" + id);
	}
}
//Partie 3: Exercice 6: les textes se remplace � chaque fin des threads et n'affiche pas les chiffres les uns � la suite des autres.
//


