package tp5;

import javax.swing.JProgressBar;

public class Cheval extends Thread implements CoureurHippique{

	//Atribut
	private int id;
	private int longueur;
	private int distance = 0;
	private JugeDeCourse juge;
	private JProgressBar bar;
	
	//Constructeur
	public Cheval(int i, int l, JugeDeCourse j, JProgressBar b) {
		this.id = i;
		this.longueur = l;
		this.juge = j;
		this.bar = b;
		
	}
	
	//Accesseur
	public int distanceParcourue() {
		return this.distance;
	}
	//M�thode
	
	
	public void run() {
		while (this.distance < longueur) {
			this.distance = this.distance + (0 + (int)(Math.random() * ((3 - 0)+1)));	//Nombre al�atoire entre 0 et 3
			System.out.println("le cheval " + this.id + " a parcouru " + this.distance + " m!");
			
			try {			//Pause de 50 ms
				Thread.sleep(50);
				bar.setValue(distance); //pour modifier la progression li� � la progressBar le programme testCheval null sera mis � la place de la progressBar
			} catch (InterruptedException e) {
				System.out.print("Erreur");
				e.printStackTrace();
			}
			
		}
		if (this.distance >= this.longueur) {
			this.juge.passeLaLigneDArrivee(this.id);
		}
	}
}
